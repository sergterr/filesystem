<?php

namespace Sergterr\Filesystem;

/**
 * Filesystem Library
 *
 * Version 1.0.6
 */
class Dir
{
    /**
     * Protected constructor since this is a static class.
     *
     * @access  protected
     */
    protected function __construct()
    {
    }

    /**
     * Creates a directory
     *
     *  <code>
     *      Dir::create('folder1');
     *  </code>
     *
     * @param $dir //Name of directory to create
     * @param integer $chmod Chmod
     *
     * @return boolean
     */
    public static function create($dir, int $chmod = 0775): bool
    {
        // Redefine vars
        $dir = (string)$dir;

        // Create new dir if $dir !exists
        return !(!self::exists($dir)) || @mkdir($dir, $chmod, true);
    }

    /**
     * Checks if this directory exists.
     *
     *  <code>
     *      if (Dir::exists('folder1')) {
     *          // Do something...
     *      }
     *  </code>
     *
     * @param string $dir Full path of the directory to check.
     *
     * @return boolean
     */
    public static function exists(string $dir): bool
    {
        // Directory exists
        if (file_exists($dir) && is_dir($dir)) {
            return true;
        }

        // Doesn't exist
        return false;
    }

    /**
     * Check dir permission
     *
     *  <code>
     *      echo Dir::checkPerm('folder1');
     *  </code>
     *
     * @param string $dir Directory to check
     *
     * @return string
     */
    public static function checkPerm(string $dir): string
    {
        // Clear stat cache
        clearstatcache();

        // Return perm
        return substr(sprintf('%o', fileperms($dir)), -4);
    }

    /**
     * Delete directory
     *
     *  <code>
     *      Dir::delete('folder1');
     *  </code>
     *
     * @param string $dir Name of directory to delete
     */
    public static function delete(string $dir)
    {
        // Delete dir
        if (is_dir($dir)) {
            $ob = scandir($dir);
            foreach ($ob as $o) {
                if ($o != '.' && $o != '..') {
                    if (filetype($dir . '/' . $o) == 'dir') {
                        self::delete($dir . '/' . $o);
                    } else {
                        unlink($dir . '/' . $o);
                    }
                }
            }
        }
        rmdir($dir);
    }

    /**
     * Get list of directories
     *
     *  <code>
     *      $dirs = Dir::scan('folders');
     *  </code>
     *
     * @param $dir
     *
     * @return array|false
     */
    public static function scan($dir)
    {
        // Redefine vars
        $dir = (string)$dir;

        // Scan dir
        if (is_dir($dir) && $dh = opendir($dir)) {
            $f = [];
            while ($fn = readdir($dh)) {
                if ($fn != '.' && $fn != '..' && is_dir($dir . DIRECTORY_SEPARATOR . $fn)) {
                    $f[] = $fn;
                }
            }

            return $f;
        }

        return false;
    }

    /**
     * Check if a directory is writable.
     *
     *  <code>
     *      if (Dir::writable('folder1')) {
     *          // Do something...
     *      }
     *  </code>
     *
     * @param $path
     *
     * @return bool
     */
    public static function writable($path): bool
    {
        // Redefine vars
        $path = (string)$path;

        // Create temporary file
        $file = tempnam($path, 'writable');

        // File has been created
        if ($file !== false) {
            // Remove temporary file
            File::delete($file);

            //  Writable
            return true;
        }

        // Else not writable
        return false;
    }

    /**
     * Get directory size.
     *
     *  <code>
     *      echo Dir::size('folder1');
     *  </code>
     *
     * @param string $path The path to directory.
     *
     * @return integer
     */
    public static function size(string $path): int
    {
        $total_size = 0;
        $files = scandir($path);
        $clean_path = rtrim($path, '/') . '/';

        foreach ($files as $t) {
            if ($t <> "." && $t <> "..") {
                $current_file = $clean_path . $t;
                if (is_dir($current_file)) {
                    $total_size += self::size($current_file);
                } else {
                    $total_size += filesize($current_file);
                }
            }
        }

        // Return total size
        return $total_size;
    }

    /**
     * Copy directory.
     * <code>
     *      Dir::copy('source_folder_path', 'destination_folder_path');
     *  </code>
     *
     * @param $src
     * @param $dst
     */
    public static function copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
